using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV1_BRESTOVAC
{
    class Note
    {
        private String text;
        private String author;
        private int importance;

        public string getText()
        {
            return this.text;
        }

        public void setText(string mText)
        {
            this.text = mText;
        }

        public string getAuthor()
        {
            return this.author;
        }

        public void setAuthor(string mAuthor)
        {
            this.author = mAuthor;
        }

        public int getImportance()
        {
            return this.importance;
        }

        public void setImportance(int value)
        {
            this.importance = value;

        }
        public string Text
        {
            get
            {
                return this.text;
            }

            set
            {
                this.author = value;
            }
        }

        public string Author
        {
            get
            {
                return this.author;
            }
        }

        public int Importance
        {
            get
            {
                return this.importance;

            }
            set
            {
                this.importance = value;
            }
        }
        public Note()
        {
            this.text = "Pozdrav!";
            this.author = "Jelena";
            this.importance = 0;

        }
        public Note(string mText, string mAuthor, int mImportance)
        {
            this.text = mText;
            this.author = mAuthor;
            this.importance = mImportance;
        }

        public Note(string mText, string mAuthor)
        {
            this.text = mText;
            this.author = mAuthor;
            this.importance = 0;
        }

        public override string ToString()
        {
            return (this.text + this.author + this.importance );
        }
    }
}
