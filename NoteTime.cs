using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV1_BRESTOVAC
{
    class NoteTime: Note
    {
        private DateTime time;

        public NoteTime(): base()
        {
            this.time = DateTime.Now;
        }
     
        public NoteTime(string mText, string mAuthor, int mImportance, DateTime mTime): base(mText, mAuthor, mImportance)
        {
            this.time = mTime;
        }

        public DateTime Time
        {
            get { return this.time; }
            set { this.time = value; }
        }

   
    }
}
