using System;

namespace RPPOON_LV1_BRESTOVAC
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1 = new Note("Brr zima", "Ivor", 1);
            Note note2 = new Note("Proljece", "Ivan");
            Note note3 = new Note();

            Console.WriteLine(note1.Author);
            Console.WriteLine(note1.Text);
            Console.WriteLine(note2.Author);
            Console.WriteLine(note2.Text);
            Console.WriteLine(note3.Author);
            Console.WriteLine(note3.Text);
            Console.WriteLine(note1.ToString());

            NoteTime time = new NoteTime();
            Console.WriteLine(time.Time);

            ListToDo toDo = new ListToDo();

            for (int i = 0; i < 10; i++)
            {
                string text = Console.ReadLine();
                string author = Console.ReadLine();
                int importance = Convert.ToInt32(Console.ReadLine());

                 toDo.AddNote(text, author, importance);
            }

            Console.WriteLine(toDo.ToString());
            toDo.RemoveHighestImportance();
            Console.WriteLine(toDo.ToString());

   
        }
    }
}
