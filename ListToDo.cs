using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV1_BRESTOVAC
{
    class ListToDo
    {
        private List<Note> notes;
        public ListToDo()
        {
            notes = new List<Note>();
        }

        public int AddNote(string mText, string mAuthor, int mImportance)
        {
            Note toDo = new Note(mText, mAuthor, mImportance);
            notes.Add(toDo);
            return notes.IndexOf(toDo);
        }
        public void RemoveNote(int index)
        {
            notes.RemoveAt(index);
        }
        public Note GetNote(int index)
        {
            return notes[index];
        }

        public override string ToString()
        {
            string notes = string.Empty;
            foreach (Note toDo in this.notes)
            {
                notes += toDo.ToString() + " ";
            }

            return notes;
        }
        private int GetHighestImportance()
        {
            int mostImportance = notes[0].Importance;
            foreach(Note toDo in notes)
            {
                if (mostImportance >toDo.Importance)
                {
                    mostImportance = toDo.Importance;
                }
            }
            return mostImportance;
        }

        public void RemoveHighestImportance()
        {
            int mostImportance = GetHighestImportance();
            for(int i=0; i< notes.Count; i++)
            {
                if (mostImportance == notes[i].Importance)
                {
                    notes.RemoveAt(i);
                    i--;
                }
            }
        }


    }
}
